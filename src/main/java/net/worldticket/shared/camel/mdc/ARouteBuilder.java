package net.worldticket.shared.camel.mdc;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.MDC;

public class ARouteBuilder extends RouteBuilder {
    public static final String MDC_KEY_CORRELATION_ID = "camel.correlationId";

    @Override
    public void configure() {
        from("direct:start")
                .process(e -> e.getIn().setBody("MDC: " + MDC.get(MDC_KEY_CORRELATION_ID)))
                .to("mock:result");
    }
}
