/*
 * Copyright (c) 2018 WorldTicket A/S
 * All rights reserved.
 */
package net.worldticket.shared.camel.mdc;

import static org.junit.jupiter.api.Assertions.assertNull;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.support.DefaultExchange;
import org.apache.camel.test.junit5.CamelTestSupport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

// Test case showing that Camel no longer clears MDC after sending a message
@SuppressWarnings({
        "java:S1874",
        "java:S100"
})
class CorrelationIdTest extends CamelTestSupport {
    private static final String MDC_KEY_CORRELATION_ID = "camel.correlationId";
    private static final String EXCHANGE_CORRELATION_ID = "FROM-EXCHANGE";

    @EndpointInject("mock:result")
    protected MockEndpoint resultEndpoint;

    @Produce("direct:start")
    protected ProducerTemplate toStart;

    @BeforeEach
    void validate() {
        MDC.clear();
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        final CamelContext camelContext = super.createCamelContext();
        camelContext.setUseMDCLogging(true);
        return camelContext;
    }

    /**
     * A factory method to create an Exchange implementation
     */
    protected Exchange createExchange() {
        return new DefaultExchange(context);
    }

    @Override
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            @Override
            public void configure() {
                from("direct:start")
                        .process(e -> e.getIn().setBody("MDC: " + MDC.get(MDC_KEY_CORRELATION_ID)))
                        .to("mock:result");
            }
        };
    }

    @Test
    void process_withoutCorrelationId() throws InterruptedException {
        // GIVEN: a holder and exchange without correlation ID

        resultEndpoint.expectedBodiesReceived("MDC: null");

        // WHEN: processing the exchange
        toStart.send(createExchange());

        // THEN: a correlation ID is generated
        resultEndpoint.assertIsSatisfied();

        // AND: MDC is cleared
        assertNull(MDC.get(MDC_KEY_CORRELATION_ID));
    }

    @Test
    void process_withCorrelationIdInExchange() throws InterruptedException {
        // GIVEN: an exchange with correlation ID
        Exchange exchange = createExchange();
        exchange.setProperty(Exchange.CORRELATION_ID, EXCHANGE_CORRELATION_ID);

        resultEndpoint.expectedBodiesReceived("MDC: " + EXCHANGE_CORRELATION_ID);

        // WHEN
        toStart.send(exchange);

        // THEN
        resultEndpoint.assertIsSatisfied();

        // AND: MDC is cleared
        assertNull(MDC.get(MDC_KEY_CORRELATION_ID));
    }
}
